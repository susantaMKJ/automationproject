
// commit test data///
/// commit test data 2nd///
//// commit/////
/////commit 4444////

package com.murcury.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class FunctionRepo {
	
	WebDriver driver;
	
	public void browserLounch()
	{
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		driver = new ChromeDriver();
	}

	public void maxBrowser() {
		
		driver.manage().window().maximize();
	}
	
	public void murcuryApplounch() {
		
		driver.get("http://newtours.demoaut.com/");
		
	}
	
	public void closeBrowser() {
		
		driver.close();
	}
	
	public void waiteFor(int timespan) throws InterruptedException {
		
		Thread.sleep(timespan);
	}
	
	public void enterUserName(String uName) {

		//WebElement txtuname = driver.findElement(By.name("userName"));  ////// by.name
		
		WebElement txtuname = driver.findElement(By.xpath("//input[@name='userName']"));   //// by.xpath
		//txtuname.sendKeys("dasd");
		txtuname.sendKeys(uName);
	}
	
	public void enterPass() {
		
		WebElement txtpass = driver.findElement(By.name("password"));
		txtpass.sendKeys("dasd");
	}
	
	public void signIn() throws AWTException {
		
		///// 1st way  
		//WebElement btnlogin = driver.findElement(By.name("login"));
		//btnlogin.click();
		
		
		//// using Robat Class///
		
		Robot rObj = new Robot();
		rObj.keyPress(KeyEvent.VK_ENTER);
	}
	
	public void departingFromSelection() {
		
		WebElement departingValue = driver.findElement(By.xpath("//select[@name='fromPort']"));   //// by.xpath
		Select se = new Select(departingValue);
		//se.selectByVisibleText("Frankfurt");  //// way 1
		//se.selectByIndex(2); /// way 2
		se.selectByValue("Portland");  /// way 3
		//departingValue.sendKeys("dasd");
	}
	
	public void verifyValidLogin() {
		
		String extectedtitle = "Find an Flight: Mercury Tours:";
		String actualtite = driver.getTitle();
		
		System.out.println(actualtite);
		
		if(extectedtitle.equals(actualtite)) {
			
			System.out.println("Success");
		}
		else {
			System.out.println("Fail");
		}
	}

}
